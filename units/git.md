# Git

Git is a (decentralized) version control program. 

> **Version control** is a system that records changes to a file or set of files over time so that you can recall specific versions later.

A **repository** is a folder that contains code that is tracked by version control. A **remote server** is a server that contains a copy of the repository. GitLab is a program to run git on your own server.

## Basic commands

To see the version of git you are using:
```
git -v 
```
For example the output will be:

>git version 2.39.2 (Apple Git-143)

To initialize a git repository (folder with your code) do:
```
git init "folder"
```
Check the status of your repository with:
```
git status
```
You can manage files with git:
```
git rm <file>
git mv <file>
```

## Git configuration

To set the git configuration such as the username, email, default editor do:

```
git config --global user.name "User Name"
git config --global user.email "email"
git config --global core.editor vim
git config --global color.ui auto
```
These information are saved in: `~/.gitconfig`. 

NOTE: If you want to change the username or email, modify directly this file and don't use the above command because there will be errors about not being able to add a second username/email.

## Commits

A commit is when you record your changes in a repository.

When modifying a file first you have to stage it. This gives you the opportunity to modify other files too before making the final decision to commit them.

To stage (track) files:
```
git add <file1> <file2> ...
```
To undo it:
```
git rm --cached <file>
git reset HEAD <file>
```
You can add (interactively) only a part of the changed code with:
```
git add -p <file>
```

If you commit but you forgot a file do:
```
git add <forgoten file>
git commit --amend
```

When commiting the changes you must ALWAYS include a commit message. If the message is short do:
```
git commit -m "my message"
```
If it needs a lot of explanation then do:
```
git commit
```
This will open your default editor. In the first top line add a short title for the commit. Then leave an empty line and you can add an entire paragraph with the explanation of the commit. Once you save and exit the editor the commit is done.

To show the differences that the commit has introduced do: ```git commit -v```.

Each commit is assosiated with a **hash** that represent it's fingerprint/unique id. You can refer to a commit with its hash.

If you make some changes, you can see the differences compared to the latest commit with:
```
git diff
```
This will give details about which lines of the files changed etc.

To see the differences between two specific commits:
```
git diff <commit1> <commit2>
```

To get general information/history about the commits (e.g. their names) of a repository:
```
git log --all
```
To see the actual changes of a specific commit (last commit by default):
```
git show <hash>
```
You can go back to a specific version with:
```
git checkout <commit name>
```
To go back to the latest commit:
```
git checkout main
```
To merge a branch in the main:
```
git merge 
```
:::{note}
To achieve that you must first checkout to main and then type the above merge command.
:::

If nothing is specified after ```git merge```, the default target is  the origin: ```origin/main```. To be more specific:
```
git merge <branch> main
```

You can tag a commit:
```
git tag -a <tag name> -m <message>
git show <tag name>
git push origin <tag name>
git checkout <tag name>
```
It is useful to tag when you are using the code to perform some analysis. It is important for others to know with which version of the code you produced the results.


### Visualization

To print the git graph structure:
```
git log --all --graph --decorate
```
or use the 'Git Graph' extension in Visual Studio Code.

Other commands:
```
git log --graph --pretty
git log --graph --pretty=oneline --decorate --abbrev-commit --all
```

## Branches

To list all of the existing branches of the repository
```
git branch -a
```
To create a new branch
```
git branch <branch_name>
```
If you are in the main branch, the new branch will be branched from there.

:::{note}
The new branch is always branched from the HEAD (your current location)
:::

To rename a branch do
```
git branch -m <old_name> <new_name>
```
To switch branches
```
git checkout <branch_name>
```
In general, you can use the ```checkout``` command to switch to a changed file, commit, branch or version of the repository.

To delete a branch (for example the main) do:
```
git branch -D main
```

## Remotes

### Cloning

Clone a repository from GitLab with:

```
git clone <git@gitlab.com:user/name.git>
```
This will clone the repository with the **ssh** cloning method, which is recommended. You will be requested to git your ssh passphrase (see instructions in the SSH keys section).

To see the origin of a cloned repository:
```
git remote -v
git remote show origin
```
To update the remore repository with your commited changes:
```
git push
```
Or more specifically:
```
git push origin <branch>
```

To get the latest version of the remote:
```
git pull
```
In reality, pull is two commands: **fetch** (getting the changes) and **merge** (combining commits from different branches).

If there are changes on the remote and you are trying to push you will get an error. However you can force it:
```
git push --force
```
BUT then everyone else is going to get an error once they try to pull and have to fix the conflict themselves locally.

There is also the force pull command:
```
git pull --force
```
or fetch/rebase to create the same histoty locally.

## Conflict management

A conflict appears when the you try to push/pull from the remote but a local file and the same file on the remote have been modified differently (at the same lines).

- always pull before pushing

## Forking

Sometimes it might be the case that you must contribute to a project but you are not allowed by the owner of the code to make direct changes to the repository. In this case you must create a **fork**, which is a copy of the project. There is no git command to fork a project, it is done on GitLab.

The official repository that you cloned is called the **upstream**.

:::{note}
When working on a fork it is recommended to keep the main branch untouched and always in sync with the upstream. Any modifications of the repository should be done in a new branch.
:::

NOTE: origin = my fork and upstream = original repository from where we only pull.

For the first push:
```
git push origin <my_branch>
```
Adding (is done once) and pulling from the upstream:
```
git remote add upstream <ssh>
git fetch upstream
git rebase upsream/main
```
NOTE: there is also a sync botton in the website of our fork that we can use instead of the above commands to keep the main synced with the upstream. We just have to sync and then pull to be up-to-date.



## SSH keys

SSH keys are is access credentials in the SSH protocol.

> **Secure Shell Protocol** = a cryptographic network protocol for operating network services securely over an unsecured network. 

SSH uses public-key cryptography to authenticate the remote computer and allow it to authenticate the user, if necessary.

### key generation

Generate the key anywhere with:
```
ssh-keygen -t ed25519 -C "email"
```
This command will generate an ed25519 type key, that is a pair of ed25519 and ed25519.pub files (identification and public key respectively) in the `~/.ssh` folder (by default if the file to be saved is not specified).

The output will be for example:
```
Generating public/private ed25519 key pair.

Enter file in which to save the key (/Users/euenikoloudake/.ssh/id_ed25519): 

Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 

Your identification has been saved in /Users/euenikoloudake/.ssh/id_ed25519
Your public key has been saved in /Users/euenikoloudake/.ssh/id_ed25519.pub

The key fingerprint is:
SHA256:p8BRjD6aqFBFy1+5eQAg6TlqY2trx1FZyvBuQtId8xs evinikoloudaki@gmail.com

The key's randomart image is:
+--[ED25519 256]--+
|  .oo..o.        |
|  .+.+.+..       |
| ..oB.O +        |
| .=o XoE +       |
| ooooo+.S o      |
|o+.ooo o +       |
|+.+ +   .        |
|.+ o             |
|o.o              |
+----[SHA256]-----+
```
Alternatively do:
```
ssh-keygen -o
```
to generate an rsa type key (id_dsa/id_dsa.pub).

### adding the key to GitLab

Go to Preferences-> SSH keys and select 'Add new key'. 

Copy the key you generated with:
```
pbcopy < ~/.ssh/id_ed25519.pub
```
and paste it in the key field on GitLab. Additionally, select a key name, it's usage type (authentication or signing) and it's expiration date. Then click 'Add key'.
