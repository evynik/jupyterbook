# Bash

## Configuring the .bashrc

Set useful aliases such as:
```
alias ls='ls --color'
alias lt='la -hltr'
alias py='python3'
alias lyon='ssh -L 9500:localhost:7400 -XY evnikolo@cca.in2p3.fr'
```

Source virtual environments:
```
source /Users/nikoloudaki/miniconda3/etc/profile.d/conda.sh
```

Set color scheme:
```
PS1="\[\033[01;32m\[\u\[\033[01;31m\]@\[\033[01;32m\]\h \W]\$ \[\033[00m\]"
```

To source the bashrc after making changes to it do:
```
source ~./bashrc
```
