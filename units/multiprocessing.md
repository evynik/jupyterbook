# Multiprocessing

Process-based parallelism.

Parallel computing: involves the usage of parallel processes or processes that are divided among multiple cores in a processor.

Usually, when you run a Python script, your code at some point becomes a process, and the process runs on a single core of your CPU. But modern computers have more than one core, so you could use more cores for your computations.

One way to achieve parallelism in Python is by using the ```multiprocessing``` module. The multiprocessing module allows you to create multiple processes, each of them with its own Python interpreter. For this reason, Python multiprocessing accomplishes process-based parallelism.

## Process class

```
from multiprocessing import Process
```
Simple example:
```
def func1():
    print("func1: starting")
    for i in range(10000000):
        pass

    print("func1: finishing")


def func2():
    print("func2: starting")
    for i in range(10000000):
        pass

    print("func2: finishing")


if __name__ == "__main__":
    p1 = Process(target=func1)
    p1.start()
    p2 = Process(target=func2)
    p2.start()
    p1.join()
    p2.join()
```
The ```Process``` class represents an activity that will be run in a separate process.

```target=func``` means that the new process will run the func function. It can also have an argument ```args=(...)``` in case the function recieves some arguments. (NOTE: if the function takes one int as an input for example, args sould be given as: args=(2,) with the comma at the end).

Once an instance to the Process class is created, we start the process. This is done by writing ```p.start()```.

Before we exit, we need to wait for the child process to finish its computations. The ```join()``` method waits for the process to terminate.

Sometimes it is important to use the multiprocessing module in order to speed up the computations. For example we may want to call the same function multiple times with different argument and save what it returns in a single array:

```
X = [1,2,3]
list = [0,0,0]

def func(i, x):
    list[i] = x
```




## Pool class

```
from multiprocessing import Pool
```




# Multithreading

