# Command line

## Inspecting files

To see the contents of a file:

- **cat** 

Simply prints the contents in the command line.

- **less**:

Scroll using the arrows and exit with ```q```. Search in the document with ```/pattern``` or ```?pattern``` and press enter. To undo the highlight press ```ESC+u```. 

## List files

List the files in a directory with **ls**.

- acoording to time of last modification: **ls -t**
- acoording to time of last access: **ls -u**
- in reverse alphabetic order: **ls -r**
- in increasing size: **ls -lSr**
- all files (including dots): **ls -a**
- all files (excluding dots): **ls -A**
- list files and their size in kilobytes: **ls -s**
- detailed information: **ls -l**

Advanced:

**ls -R**: it will list all files as well as the subdirectories together with their contents.

## Counting files

To see how many files there are in a folder do:
```
ls -1 | wc -l
```

## Find file location

To find the location of a file do:
```
find /path -name "file.py"
```
This will search for the name ```file.py``` in the specified folder and will return its absolute path.

To see today's files (last 24 hours) do one of the following:
```
find <path> -daystart -ctime 0 -print

find . -mtime -1 -type f -print
```

To see only files from a specific date do:
```
ls | grep 'Mar  1'
```
Leave two spaces between the month and day.

## Find pattern in file

To find a the file that contains a specific pattern, for example the function ```get_metadata```, in a folder do:
```
grep -Rnw /path/to/directory -e "get_metadata"
``` 
This will list the path and name of the files that contain this pattern, as well as the line and how it appears.

## Links

### Symbolic links

In computing, a symbolic link (also symlink or soft link) is a file whose purpose is to point to a file or directory (called the "target") by specifying a path thereto.

To create a symlink do:
```
ln -s /target/path /link/path or name
```
A name will appear in the working directory or specified path that will connect the desired folder or file. Then we can just cd to the folder directly from our working directory.

To remove it:
```
ln -l /target/path /link/path
unlink <path-to-symlink>
rm <path-to-symlink>
```
Note: the path or name of the link must not end with / when trying to remove it!

To find broken links:
```
find /path -xtype l
```

## Permissions

The **chmod** command modifies the mode bits and the extended access control lists (ACLs) of the specified files or directories.

Useful lags:

- u: file owner
- g: group
- o: others
- -: removes specified permissions
- +: applies specified permissions
- r: read permission
- W: write permission

Example: give permission to the user, group and others to recieve reading and writing rights:

```
chmod ugo+rw example.txt
```
or to quickly have permission to your file if permission is denied do:
```
chmod 777
```

## Memory management

To see how much memory you are using do:
```
du -h -d 1
```
This will show:

![im](images/Screenshot_2024-03-04_at_15.02.56.png)

which means that the total memory that I use is 1.1G.

### Monitor memory usage
htop command in Linux system is a command line utility that allows the user to interactively monitor the system’s vital resources or server’s processes in real time.
```
htop -u username
```
![im](images/Screenshot_2024-03-01_at_10.43.07.png)


The information shown is the following:

- PID:                 Unique Process ID.
- USER:             Process Owner.
- NI: The nice value of the process, which affects its priority.
- VIRT:               Virtual memory being consumed by the process
- CPU:            The percentage of the processor time used by the process.
- MEM:           The percentage of physical RAM used by the process.
- RES: How much physical RAM the process is using, measured in kilobytes.
- SHR: How much shared memory the process is using.
- S: The current status of the process (zombied, sleeping, running, uninterruptedly sleeping, or traced).
- TIME+: How much processor time the process has used.
- COMMAND:   The name of the command that started the process.

VIRT stands for the virtual size of a process, which is the sum of memory it is actually using, memory it has mapped into itself (for instance the video card's RAM for the X server), files on disk that have been mapped into it (most notably shared libraries), and memory shared with other processes. VIRT represents how much memory the program is able to access at the present moment.
RES stands for the resident size, which is an accurate representation of how much actual physical memory a process is consuming. (This also corresponds directly to the %MEM column)
SHR indicates how much of the VIRT size is actually sharable memory or libraries.

The numbers on the top left from 1 to 40 represents the number of cpu's/cores in my system with the progress bar next to them representing the load of cpu/core. As you would have noticed the progress bars can be comprised of different colors. The following list will explain what each color means.

Blue: low priority processes (nice > 0)
Green: normal (user) processes
Red: kernel processes
Yellow: IRQ time
Magenta: Soft IRQ time
Grey: IO Wait time


To save the output do:
```
htop -d 10 -n 1 -b > htop_output.txt
```

### Memory leakages

Detect memory leakages with valgrind.

It might be required to change the permissions of the file under investigation: ```chmod a+x file.py```.

Then run 
```
valgrind --leak-check=full --log-file="logfile.out" -v --track-origins=yes file.py
```

## Virtual environments

### Conda/mamba

To create a conda venv follow the steps:

1. Install miniconda or anaconda
2. Activate the base with:
```python
conda activate base
```
3. Install mamba:
```python
conda install mamba -c conda-forge
```
4. Create a conda.yml file with the libraries you want to install. For example:
```python
name: venv
channels:
  - conda-forge
  - defaults
dependencies:
  - awkward=1.10.1
  - iminuit=2
  - matplotlib
  - pip
  - pip:
    - scipy>=1.9.1
```
5. Create the venv and install its libraries:
```python
mamba env create --file conda.yml
```
6. Activate the virtual environment:
```python
conda activate venv
```

To list all if the available virtual environments in your computer do:
```
conda env list
```

## Python server

This is useful for sharing local files with people that are connected on the same network.

First go to the directory with the files you want to share and start a server with the following command:
```python
python3 -m http.server 8080
```
8080 is the port number. To kill the server do control-c.

To access the server online, one must search for:
```
IP:8080
```
where IP is the IP address of the computer who started the server. The files and downloading is available as long as the server is active. Once it is killed, other users can only access files they had already opened (they have been cached).

