# User inputs

## Argparse
Parser for command-line options, arguments and sub-commands. 

Basic syntax:
```
parser = argparse.ArgumentParser()
parser.add_argument('-i','--input',  type=str)
args = vars(parser.parse_args()) 

par = args['input']
```

```add_argument``` links:

- ***action***: Specify how an argument should be handled. Options: 'store', 'store_const', 'store_true', 'append', 'append_const', 'count', 'help', 'version'
- ***choices***: Limit values to a specific set of choices.
- ***const***: Store a constant.
- ***default***: Default value used when an argument is not provided.
- ***dest***: Specify the attribute name used in the result namespace.
- ***help***: Help message for an argument.
- ***metavar*** : Alternate display name for the argument as shown in help.
- ***nargs***: Number of times the argument can be used. Options: (int, '?', '*', or '+')
- ***required***: Indicate whether an argument is required or optional.
- ***type*** 	
Automatically convert an argument to the given type ment to the given type. Options:
int, float, argparse.FileType('w'), or callable function



## Docopt
