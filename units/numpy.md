# Numpy

## Data types

NumPy numerical types are instances of dtype (data-type) objects. There are 5 basic numerical types representing booleans (bool), integers (int), unsigned integers (uint) floating point (float) and complex. Those with numbers in their name indicate the bitsize of the type (i.e. how many bits are needed to represent a single value in memory). 

Data-types can be used as functions to convert python numbers to array scalars, python sequences of numbers to arrays of that type, or as arguments to the dtype keyword that many numpy functions or methods accept. 

To see the data type of a variable:
```
var.dtype
```
and to set the type
```
var = np.dtype(int8)
z = np.arange(3, dtype=np.uint8)
```
*The fixed size of NumPy numeric types may cause overflow errors when a value requires more memory than available in the data type.*
To see the bounds:
```
np.iinfo(int)
```


## numpy histograms

For a simple 1d histogram:
```
h, bins = np.histogram(data, bins = n, range=()) 
```
The data must be an array. It returns h, that is an array which contains the number of events in each bin and bins, that are the bin edges.

To plot it 
```

```
