# Vim

Henrique's weird configuration uses `space` as `leader`

PS.: when you see things with `)` it also works for `]`, `}` and others

## Exiting

- `:w` save
- `:wq` or `:x` or `ZZ` save and quit
- `:wqa` save and quit all
- `:e file_name` open a file named `file_name` (IF there is no file, it will be created when saved `:w`)
- `leader .` shortcut for `:e`


## Editing

- `i` enter insert mode
- `I` enter insert mode at the first character of the line
- `A` enter insert mode at the end of the line
- `cw` delete word from this point and enter insert mode 
- `ciw` delete entire word and enter insert mode
- (in visual mode) `c` delete selected and enter insert mode
- `C` insert from this point, delete all in front
- `c i )` will delete everything inside the parenthesis and enter insert mode
- `c a )` same but including the parenthesis
- `d i )` same but keep in normal mode
- `d a )`
- `jk` exit insert mode
- `x` delete character
- `dd` delete line
- `2dd` delete two lines etc
- `Ctrl-n` for auto complete (`Ctrl-p` after to cycle back), `Tab` to choose
- `r` replace character
- `R` continuous replace (same as insert) 
- `u` undo 
- `Ctrl-r` redo
- `Ctrl-x u` open/close undotree
- `.` repeat last command
  - Example: if you enter insert mode and type some text. After you are back to normal mode, if you type `.` it will repeat your insertion. Works for MANY things
- `:%s/old/new/g` replace old with new in the whole document. You can change `/` by `#`.
  - Select first to choose area



## Coping and pasting

yank = copy

### In normal mode
- `yy` copy line
- `y$` yank from this point to the end of the line
  - Also `y g _` to not copy linebreak
- `yw` copy word 
- `y i )` copy everything inside parenthesis
- `y a )` same but including parenthesis
- `p` paste 
- `P` paste before
- `"*y` or `"+y` yank (copy) to system clipboard
- `"*p` or `"+p` paste from system clipboard
- Print all registers: `:reg`
  - `"np` paste from register `n`
  - `"ny` copy to register `n`

### Visual mode
- `v` enter visual mode
- `v i )` enter visual mode selecting everything inside parenthesis
- `V` enter visual line mode (select entire line)
- `Ctrl-v` enter visual block mode
- `y` copy
- `"*y` or `"+y` yank (copy) to system clipboard
- `"ny` copy to register `n`

### Paste mode

If pasting with ctrl-v in insert mode, type `:set paste` first
After finish pasting type `:set nopaste`

## Navigating

- `w` jump word
- `W` jump word including characters
- `b` backward one word
- `B` backward one word including special characters
- `0` start of line
- `$` end of line
- `g _` end of line without break
- `fc` in the same line go forward to the word that has a 'c'
- `Fc` in the same line go backward to the word that has a 'c'
- `tc` same as `fc` but place before character
- `gg` first line
- `G` last line
- `23 gg` go to line 23
- `:{number}` go to line number
- `#` or `*` highlight text matches under cursor 
- `Ctrl-g` or (`:nohl`) to remove highlights) 

## Windows

- `leader w v` split window vertically
- `leader w s` split window horizontal
- `leader w c` closes window
- `leader w o` close all other windows
- `leader w h, j, k or l` navigate
- `leader w H, J, K or L` change windows position

## Buffer

Buffers are basically your open files 

- `leader b b` list all open buffers and choose which one
- `leader b l` go back to last buffer
- `leader b k` close buffer (same as `:bd`)
    - If there was a change and you don't want to save. Use `:bd!`

## EasyAlign
Select lines you want to align with visual mode.
- `ga` to align , choose point (ex: `=`)

## Comment:
- `g c c` (un) comment line
- (on visual mode) `g c` (un) comment

## vim surround
Check it here: [https://www.futurile.net/2016/03/19/vim-surround-plugin-tutorial/](https://www.futurile.net/2016/03/19/vim-surround-plugin-tutorial/)

## Multi-line editing

- Select the block you want to edit with `Ctrl-v`:
- Press `I` to just start texting 
- Press `C` to delete the block and enter insert mode

## Vim recording

`q a` start recording at register `a` (any letter). Stop recording with `q` (press `Esc` first for safety)
- `@a` to executed what you recorded 

## Tabs

- `:tabedit file` edit file in a new tab
- `:tabclose` close tab
- `g t` go to tab on the right
- `g T` go to tab on the left
