# tmux

## Installation

First install tmux based on your operating system.

Then clone this git for the plugins:
```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```


## Cheatsheet

Ctrl-s is `prefix` or `leader` (meaning Ctrl-b)

Detach: `Ctrl-s d`
Rename session: `Ctrl-s $`
List sessions:
  `tmux ls`
  `tmux list-sessions`
  `Ctrl-s s`

Start a new session or attach to an existing session named mysession:
  `tmux new-session -A -s mysession`

Attach to session:
  `tmux a -t mysession`

+ Reload tmux config: `Ctrl-s r`

+ Install plugin: `Ctrl-s I`

+ Panes
  - New pane:
      Horizontal split: `Ctrl-s %`
      Vertical split: `Ctrl-s "`
  - Close pane: `Ctrl-s x`
  - Move around: `Ctrl-s h,j,k or l`
      You can also use arrows
  - Resize: `Ctrl-s H,J,K or L`
  - Swap: `Ctrl-s > or <`
  - Change styles: `Ctrl-s space`
  - Sync panes: `Ctrl-s ctrl-x`
  - Zoom on pane: `Ctrl-s z`

+ Windows
  - New window: `Ctrl-s c`
  - Close window: `Ctrl-s &`
  - Rename window: `Ctrl-s ,`
  - Move around: `Ctrl-s ctrl-h, ctrl-l`
  - Go to last window: `Ctrl-s Tab`

+ Copy and paste stuff
  Enter copy-mode: `Ctrl-s [`
  - In copy-mode:
    - Move around: `h j k l`
    - Select: `v`
    - Select line: `V`
    - Select block: `Ctrl-v`
  - Copy: `y` or `Enter`

  Outside of copy-mode:
  - Paste: `Ctrl-s p`
  - Choose from buffer: `Ctrl-s P`



## Config

Create a ~/.tmux.conf file and add the following:

```
unbind r
bind r source-file ~/.tmux.conf

set -g prefix C-s
set -g status-position top
set -g default-terminal "xterm-256color"
setw -q -g utf8 on
set -g default-command "/bin/bash"
setw -g mode-keys vi

# pane navigation
bind -r h select-pane -L  # move left
bind -r j select-pane -D  # move down
bind -r k select-pane -U  # move up
bind -r l select-pane -R  # move right
bind > swap-pane -D       # swap current pane with the next one
bind < swap-pane -U       # swap current pane with the previous one

# pane resizing
bind -r H resize-pane -L 2
bind -r J resize-pane -D 2
bind -r K resize-pane -U 2
bind -r L resize-pane -R 2

# Reduce time to wait
set-option -g repeat-time 350

# mouse mode
# Toggle mouse on/off
set -g mouse on
bind m set -gF mouse "#{?mouse,off,on}"

# window navigation
unbind n
unbind p
bind -r C-h previous-window # select previous window
bind -r C-l next-window     # select next window
bind Tab last-window        # move to last active window

# for better split in same dir
bind c new-window -c "#{pane_current_path}"
bind '"' split-window -c "#{pane_current_path}"
bind % split-window -h -c "#{pane_current_path}"



# -- copy mode -----------------------------------------------------------------

bind Enter copy-mode # enter copy mode

bind -T copy-mode-vi v send -X begin-selection
bind -T copy-mode-vi C-v send -X rectangle-toggle
bind -T copy-mode-vi y send -X copy-selection-and-cancel
bind -T copy-mode-vi Escape send -X cancel
bind -T copy-mode-vi H send -X start-of-line
bind -T copy-mode-vi L send -X end-of-line

# -- buffers -------------------------------------------------------------------

bind b list-buffers     # list paste buffers
bind p paste-buffer -p  # paste from the top paste buffer
bind P choose-buffer    # choose which buffer to paste from


# List of plugins
set -g @plugin 'tmux-plugins/tpm'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'github_username/plugin_name#branch'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'


set -g @plugin 'dracula/tmux'

set -g @dracula-show-powerline false
# available plugins: battery, cpu-usage, git, gpu-usage, ram-usage, network, network-bandwidth, network-ping, weather, time
set -g @dracula-plugins "cpu-usage ram-usage weather time"
# available colors: white, gray, dark_gray, light_purple, dark_purple, cyan, green, orange, red, pink, yellow
# set -g @dracula-[plugin-name]-colors "[background] [foreground]"
set -g @dracula-show-powerline false
set -g @dracula-fixed-location "Paris"
set -g @dracula-cpu-usage-colors "pink dark_gray"
set -g @dracula-show-fahrenheit false
set -g @dracula-show-flags true
set -g @dracula-show-left-icon session
set -g @dracula-military-time true


# Address vim mode switching delay (http://superuser.com/a/252717/65504)
set -s escape-time 0

# Increase scrollback buffer size from 2000 to 50000 lines
set -g history-limit 50000

# Increase tmux messages display duration from 750ms to 4s
set -g display-time 4000

# Refresh 'status-left' and 'status-right' more often, from every 15s to 5s
set -g status-interval 5


# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

bind C-x set synchronize-panes

```
