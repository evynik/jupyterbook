# Probability density functions

A probability density function or pdf, is a function that gives the probability of each value of a parameter to occur. It has two characteristics:
- it is positive everywhere
- it is normalised to 1, which means that its integral over the entire range is equal to one.

They can be discrete or continuous (more important).

Commonly used pdfs in Physics are described below.

# Poisson

The probability of observing k events when we expect λ is:

$$P(k|λ) =\frac{\lambda^k e^{-\lambda}}{k!}$$

It is often the case that: $ \lambda = rate \cdot t $

The probability to observe 0 events is

$$ P(k=0) = e^{-\lambda}$$

and following, the probability to observe at least one event is:

$$ P(k>1) = 1 - e^{-\lambda} $$

The probability to observe exactly one event is:

$$ P(k=1) = \lambda e^{-\lambda}$$

