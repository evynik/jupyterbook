# Google Cloud

Follow the instructions to create a Google Cloud project here: https://developers.google.com/workspace/guides/get-started


## APIs

In your project enable the Google Drive and Google Spreadsheets APIs. Then download the .json file that is created when you generate the key.

### Google Spreadsheets 

First create a Google Spreadsheet and share it with your project's email. 

Do the following imports:

```python
import json
import pandas as pd
import gspread
import gspread_dataframe as gd
from oauth2client.service_account import ServiceAccountCredentials
```
and then add to your script the following function in order to connect with the spreadsheet.

```python
def get_excel(name):
    scope = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name('<your key file>.json',scope)
    client = gspread.authorize(creds)
    sheet = client.open(name)
    return sheet
```

When you have a pandas dataframe in your script you can add it to the spreadsheet as follows:
```python
sheet = get_excel('<name of spreadsheet>')
sheet_id = 0
sh = sheet.get_worksheet(sheet_id)
sh.clear()
gd.set_with_dataframe(worksheet=sh,dataframe=df,include_index=True, include_column_header=True,resize=True)
```