# Errors

There are (at least) two distinguishable kinds of errors: syntax errors and exceptions. Exceptions are errors detected during execution. Some of the types are: ZeroDivisionError, NameError and TypeError.

## Handling exceptions

Using the `try` statement:

```python
while True:
    try:
        x = int(input("Please enter a number: "))
        break
    except ValueError:
        print("Not a valid number.")
```
If no exception occurs, the except clause is skipped and execution of the try statement is finished. If an exception occurs during execution of the try clause, the rest of the clause is skipped. Then, if its type matches the exception named after the except keyword, the except clause is executed, and then execution continues after the try/except block. If an exception occurs which does not match the exception named in the except clause, it is passed on to outer try statements; if no handler is found, it is an unhandled exception and execution stops with an error message.

A try statement may have more than one except clause, to specify handlers for different exceptions. At most one handler will be executed.

An except clause may name multiple exceptions as a parenthesized tuple:
```python
 except (RuntimeError, TypeError, NameError):
     pass
```

