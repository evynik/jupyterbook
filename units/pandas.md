# Pandas

Pandas offers two types of classes: series and dataframes. The difference is that series are one dimensional in contrast to the dataframes.

## Pandas basics

```
import pandas as pd
```

A basic pandas dataframe is:
```
df = pd.DataFrame(
    {
        "A": 1.0,
        "B": "hello",
        "C": np.array([3]*4, dtype="int32")
    }
)
```
It holds a dictionary where the keys are the column names and the values the column values.

- To create a numpy array and remove the names of columns/rows
```
df.to_numpy()
```
- To transpose the data
```
df.T
```


### Viewing the data

- To see the entire dataframe
```
print(df)
```
- To see the column names
```
df.columns
```
- For a statistic summary of the data
```
df.describe()
```

### Getting items

Select a column of the dataframe using its name
```
df["A"]
```
To get the dataframe without specific columns
```
df[~df["A","B"]]
```
To select a certain range of rows
```
df[0:3]
```
The above will select the first three rows (the 3 is not included). Row ranges can also be specified using the row labels.

- To select rows by label:
```
df.loc["row label"]
df.loc[:, ["A","B"]]
```
The first returns only one row, while the second returns all rows for only the columns A and B.

Another fast method that can get a sigle scalar value of the dataframe is:
```
df.at("row label", "column label")
```

- To select rows by position, for example the nth row
```
df.iloc[n]
```
- To get certain rows and columns
```
df.iloc[[1,2,4], [0,2]]
```
## Dataframe modifications

To make a copy of the original dataframe 
```
df2 = df.copy()
```
To add a new column
```
df["new column"] = ["name", value1, value2, ...]
```
The number of elements must match the number of rows that exist in the dataframe.

To add a new row at the end of the dataframe
```
df.loc[len(df)] = ["row name", value1, ...]
```
To directly add another row after this one, in the same dataframe, do ```df.loc[len(df)+1]```. Without the +1 the last row will be overwritten since it is the same dataframe. Another way would be to copy the initial dataframe, get the length of the copy and add there the new row.

To set a new value
```
df.at["row", "column"] = a
```

### Missing data

To see if there are nans (returns boolean)
```
pd.isna(df)
```
To remove all rows that contain missing data
```
df.dropna(how='any')
```
To substitute nans with a value
```
df.fillna(value=1)
```


## Selections

To select rows based on a (boolean) condition
```
sel = df['A'] > 0
df[sel]
```
To filter with ```.isin()```
```
sel = df["A"].isin(["value1", "value2"])
df[sel]
```

## Operations

Some useful dataframe operations are:
```
df.mean()
df.mean(axis=1)
df.sum()
df.sub(df2,axis="index")
df.value_counts()
```
To put the columns on top of eachother
```
stacked = df.stack(future_stack=True)

unstacked = stacked.unstack()
```
To multiply a column with a scalar
```
df.name *= number

df["name"] *= number
```
Where name is the name of the column.

### String methods

To replace the column titles
```
df.columns = df.columns.str.replace(" ", "", regex=False)
```
To make all lowercase
```
df.columns = df.columns.str.lower()
```
## Reading csv

To read a .csv file
```
df = pd.read_csv("file.csv")
```
To convert a dataframe into a csv file
```
df.to_csv("file.csv")
```

## Importing from excel document

To read from an online excel document we need the document id (from its url), the sheet id and to specify the format to be exported:
```
doc_id = "..."
sheet_id = "0"

url = f'https://docs.google.com/spreadsheets/d/{doc_id}/export?format=csv&gid={seet_id}'

df = pd.read_csv(url)
```
To convert a dataframe into an excel file
```
df.to_excel("name.xlsx", sheet_name="Sheet1")
```
To read a regular excel file
```
pd.read_excel("name.xlsx", "Sheet1")
```

## Plotting dataframes

```
plt.figure()
df.plot()
plt.legend(loc="name")
```

