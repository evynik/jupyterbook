# iminuit

```
from iminuit import Minuit
```

## Model
First we define the model/pdf that will describe our data. It will depend on some observables and on the parameters we need to define with the fit.

Simple model example:
```
def line(x, α, β):
    return α + x * β
```

To check the current parameter values and settings:
```
m.params

for p in m.params:
    print(repr(p), "\n")
```

## Cost functions

iminuit can be used with a user-provided cost functions in form of a negative log-likelihood function or least-squares function. 

Common standard functions:
```
from iminuit.cost import UnbinnedNLL
from iminuit.cost import LeastSquares
```

- **LeastSquares**: its cost function computes the sum of squared residuals between the model and the data

The cost function must be twice differentiable and have a minimum at the optimal parameters.

Simple example:
```
data_x = np.linspace(0, 1, 10)
data_yerr = 0.1 
rng = np.random.default_rng(1)
data_y = rng.normal(line(data_x, 1, 2), data_yerr)

least_squares = LeastSquares(data_x, data_y, data_yerr, line)
```

## Minimizing

For the simple example:
```
m = Minuit(least_squares, α=0, β=0)  # starting values for α and β

m.migrad()  # finds minimum of least_squares function
m.hesse()   # accurately computes uncertainties
```
The main algorithm MIGRAD is a local minimizer. It searches for a local minimum by a doing a mix of Newton steps and gradient-descents from a starting point. If your function has several minima, the minimum found will depend on the starting point. Even if it has only one minimum, iminuit will converge to it faster if you start in the proximity of the minimum.

To set limits for the parameters:
```
m.limits = [(0, None), (0, 10)]
```

### Fixing and releasing

Sometimes you have a parameter which you want to set to a fixed value temporarily. Perhaps you have a guess for its value, and you want to see how the other parameters adapt when this parameter is fixed to that value. Or you have a complex function with many parameters that do not all affect the function at the same scale. Then you can manually help the minimizer to find the minimum faster by first fixing the less important parameters to initial guesses and fit only the important parameters. Once the minimum is found under these conditions, you can release the fixed parameters and optimize all parameters together. Minuit remembers the last state of the minimization and starts from there. The minimization time roughly scales with the square of the number of parameters. Iterated minimization over subspaces of the parameters can reduce that time.
```
m.fixed["α"] = True
m.migrad()
m.fixed["α"] = False

m.fixed["β"] = True
m.migrad()

m.fixed = False
m.migrad()
```

```
m.minos()
```


## Results

```
m.parameters
m.values
m.values["α"]
m.errors
m.merrors
m.fval
m.ndof
m.fmin.reduced_chi2
```

```
for key, value in zip(m.parameters, m.values):
    print(f"{key} = {value}")
```

```
m.fmin # info about the function minimum
print(repr(m.fmin))
```
The most important one here is is_valid. If this is false, the fit did not converge and the result is useless. If the fit did not converge, is_above_max_edm is true.

```
m.covariance
m.covariance.correlation() 
```

## Plotting

To plot the fitted model:
```
plt.plot(data_x, line(data_x, *m.values))
```
To include fit results in the plot legend:
```
fit_info = [
    f"$\\chi^2$/$n_\\mathrm{{dof}}$ = {m.fval:.1f} / {m.ndof:.0f} = {m.fmin.reduced_chi2:.1f}",
]
for p, v, e in zip(m.parameters, m.values, m.errors):
    fit_info.append(f"{p} = ${v:.3f} \\pm {e:.3f}$")

plt.legend(title="\n".join(fit_info), frameon=False)
```

```
# draw matrix of likelihood contours for all pairs of parameters at 1, 2, 3 sigma
m.draw_mnmatrix();
```

```
# draw three confidence regions with 68%, 90%, 99% confidence level
m.draw_mncontour("α", "β", cl=(0.68, 0.9, 0.99));
```

```
m.draw_mnprofile("α");
```
