# ROOT at the command line

Some useful commands when using ROOT from the command line.

- To quickly inspect the contents of the root file (e.g the name of the trees):
```
.ls
```
## Trees

To see all the leafs of a tree, show the values of the first event:
```
tree->Show(0)
```
To see all the values of all or some leafs (from all of the events):
```
tree->Scan()
tree->Scan("leaf1:leaf2:leaf3")
```
Operations on tree values:
```
tree->GetMaximum("leaf")
```
Drawing:
```
tree->Draw("leaf")
```

## Drawing 

Following the tree paragraph, we can simply draw a one-dimensional histogram such as:
```
tree->Draw("var")
```
This will create a temporary histogram called htemp. To change its name and the binning do:
```
tree->Draw("var>>hist(100, 0, 100)")
```
This will create a histogram called hist with 100 bins from 0 to 100.

To apply some selections:
```
tree->Draw("var>>hist(100, 0, 100)", "var > 0 && var < 200")
```
For a two dimensional histogram do:
```
tree->Draw("var1:var2>>hist(100, 0, 100, 100, 0, 100)", "", "colz")
```
If there are no selections add an empty "" defore the last option. 

### Labels

To add a title to the histogram with name h:
```
h->SetTitle("title")
```
For the x and y labels:
```
h->GetXaxis()->SetTitle("X axis title")
h->GetYaxis()->SetTitle("Y axis title")
```
To add a legend:
```
auto legend = new TLegend(0.1,0.7,0.48,0.9)
legend->AddEntry(h, "Histogram \"h\"", "l")
legend->Draw()
```
Where the numbers in the parenthesis correspond to (xlow, ylow, xhigh, yhigh). 0 is the start of the axis and 1 the end from left to right for x and from down to up for y.

### Lines, markers and colors

:::{note}
To modify the cosmetics of the plot the order of the commands should be the following:
```
tree->Draw("var>>h")
h-> ...modification...
h->Draw()
```
:::


For the marker or line color:
```
h->SetLineColorAlpha(kBlue, 0.35)
h->SetMarkerColorAlpha(kBlue, 0.35)
```
The 0.35 refers to the transparency of the line or marker. (It can be omitted by removing the 'Alpha')

Available color ids:
![colors](images/Screenshot_2024-02-28_at_18.09.39.png)

### Multiple plots

To plot many histograms with different styles on the same canvas, follow the example below:

```
tree->Draw("x1>>h1")
tree->Draw("x2>>h2", "same")

h1->SetMarkerColor(2)
h1->Draw("same")

h2->SetMarkerColor(1)
h2->Draw("same")
```

## Fitting

