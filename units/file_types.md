# File types and operations

# File object

`open()` returns a file object, and is most commonly used with two positional arguments and one keyword argument:
```python
f = open('workfile', 'w', encoding="utf-8")
f.close()
```
*mode* can be `'r'` when the file will only be read, `'w'` for only writing (an existing file with the same name will be erased), and `'a'` opens the file for appending; any data written to the file is automatically added to the end. `'r+'` opens the file for both reading and writing. Appending a `'b'` to the mode opens the file in binary mode. Binary mode data is read and written as bytes objects (you can not specify encoding).

To ensure that the file properly closes after its suite finishes use `with`:
```python
with open('workfile', encoding="utf-8") as f:
    read_data = f.read()
```

NOTE: Calling `f.write()` without using the with keyword or calling `f.close()` might result in the arguments of `f.write()` not being completely written to the disk, even if the program exits successfully.

To read a file:
```python
for line in f:
    print(line, end='')

or 

f.readlines()

or 

list(f)
```
`f.write(string)` writes the contents of string to the file, returning the number of characters written.

`f.tell()` returns an integer giving the file object’s current position in the file represented as number of bytes from the beginning of the file when in binary mode and an opaque number when in text mode.

# txt

To read a .txt file
```python
np.loadtxt(input)
```

To write a .txt file
```python
np.savetxt("path/name.txt", input)
```
For counting the number of lines in the file use the subprocess library. 
```python
import subprocess

n = int(subprocess.check_output(['wc', '-l', file]).split()[0])
```
## Read and modify

To read a text file and modify it by creating a new one:
```python
with open(file, 'r') as f:
    lines = f.readlines()
    new_lines = outs
    for i, l in enumerate(lines):
        outs[i] = ...

with open(new_file, 'w') as n:
    for l in outs:
        n.write(l)
```
Add the following commands:

- to get the last line:
```python
last_line = f.readlines()[-1]
```
- to find a match in the text file:
```python
import re

m = re.match('.*name pattern*.', l)
if m: ...
```

# csv

csv files can be easily read as a pandas dataframe:

```python
f = 'file.csv'
df = pd.read_csv(f)
```

# JSON

To read a .json file (JavaScript Object Notation)
```python
import json

data = json.load(open("path/name.json", "r"))

for key,value in data.items():
    a = value['input1']
    b = value['input2']
```
To save a dictionary as a .json file:
```python
json.dump(dict, open("name.json", "w"))
```
The standard module called json can take Python data hierarchies, and convert them to string representations; this process is called serializing. Reconstructing the data from the string representation is called deserializing. Between serializing and deserializing, the string representing the object may have been stored in a file or data, or sent over a network connection to some distant machine.

# npz

To load an npz file
```python
npzfile = np.load("path/name.npz")
```
and to access its contents
```python
input1 = npzfile["arr_0"]
input2 = npzfile["arr_1"]
```
To see the name of the inputs:
```python
print(npzfile.files)
```

Writing .npz file
```python
np.savez(name, input1, input2, input3, ...)
```
The name of the file does not need to have .npz at the end since it is added automatically if it does not exist.

To save the inputs by giving them specific names instead of 'arr_0'
```python
np.savez(name, input='input1')
```
