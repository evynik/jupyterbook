# os, paths and files

A collection of packages that help with file management and code automatization.

# os

Miscellaneous operating system interfaces.

```
import os
```

Most commonly used commands:

- To execute a command within a script
```
os.system("your command")
```
- To get the current working directory
```
os.getcwd()
```
- To change into another directory
```
os.chdir("path to directory")
```
- To create a directory
```
os.mkdir("path to new directory")
```
- To remove a file or a directory
```
os.remove("path to file")
os.rmdir("path to directory")
```
- To scan the contents of a directory
```
os.scandir("path")
scandir.close()
```
- To find the commmon part between various strings in a list
```
os.path.commonprefix(['name1', 'name2'])
```
The names should not be paths. If it is a list of paths we need to convert them first to names:
```
os.path.commonprefix([f.split("/")[-1] for f in list])
```

Other commands:

```
os.is_dir()
os.is_file()
os.path.getctime()
os.path.split()
os.sync()
os.times()
```


# Paths and files

- To create a directory if it doesn't exist
```
if not os.path.exists("path"): os.mkdir("path")
```
- To join different paths or paths and a file
```
os.path.join(path1,path2,..,file)
```

## shutil
High-level file operations. Especially for moving, copying and removing files.
```
import shutil
```
- To move a file in a new location
```
shutil.move(path1/file, path2/file)
```
If in the destination directory (path2) the file already exists, an error will be raised since it can not be overwritten (ERROR: file already exists...). So it is better to add the file name after the destination directory to allow it to be overwritten.

## pathlib
Object-oriented filesystem paths. This module offers classes representing filesystem paths with semantics appropriate for different operating systems.

```
from pathlib import Path
```

- To list subdirectories
```
p = Path('.')
dirs = [x for x in p.iterdir() if x.is_dir()]
```
- To generate filenames by walking in a directory top-down or bottom-up
```
Path.walk(top_down=True, on_error=None, follow_symlinks=False)
```
- Initializing a path
```
path = Path('setup.py')
```
- To create a new directory in the path
```
Path.mkdir()
```
- To open and read a file in the path
```
p = Path('setup.py')
with p.open() as f:
    f.readline()
```
- To make an absolute path (e.g /home/...)
```
p.absolute()
```
- To remove an empty directory
```
Path.rmdir()
```
- To check if a certain path points to the same file as another path
```
p = Path('p1')
q = Path('p2')
p.samefile(q)
False
p.samefile('p1')
True
```
- To create a file at a given path
```
Path.touch()
```
- Common commands:
```
Path.cwd()
Path.home()
Path('setup.py').exists()
sorted(Path('.').glob('**/*.py'))
Path.is_dir()
Path.is_file()

```
## glob
Unix style pathname pattern expansion. The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell, although results are returned in arbitrary order.

- To have a list of all files in a directory:

```
files = glob.glob(f'{input_path}/*')
```
Use `np.sort(files)` to have them sorted according to name.

To count them: `len(files)` since files is a list of paths to files.

## PYTHONPATH

PYTHONPATH is a special environment variable that shows the Python interpreter where to find various libraries and applications. The syntax is the following:
```
PYTHONPATH=path python3 file.py
```
This way we can do imports in our script that exist in the specified path.

We can also do
```
export PYTHONPATH=path
python3 file.py
```
To include the path inside the file, without specifying it in the terminal:
```
sys.path.insert(0, os.path.abspath(path))
```
and to append an additional path to PYTHONPATH:
```
sys.path.append(os.path.abspath(path))
```

# Functions

Some useful functions that use the above methods.

1) Get the most recent file in directory

```
file_pattern = os.path.join(path,"pre"+"*"+".txt")

latest_file = max(glob.iglob(file_pattern), key=os.path.getctime)
```



