# ML basics

Introduction to machine learning mathematics and definitions.

## Useful definitions

General definitions:

- **samples**: the input data points.
- **label**: a class (category) associated to a sample.
- **target**: the truth. It is what the model should ideally have predicted.
- **training set**: the data that the model learns from.
- **test set**: the data on which the model is being tested. 
- **parameters**: the network's weights.
- **hyperparameters**: the number and the size of the layers.
- **layer**: a data-processing module or filter that extracts representations (encodings) from the input data.
- **learning**: the automatic search process of better representations.
- **depth**: the number of layers in a model.
- **deep learning**: a model with tens of successive layers of representations.
- **weight**: numbers that help the network to map the input data to the correct outputs. The weights are initially random, but are updated throughout the training loop, so that they minimize the loss function.
- **model's capacity**: the number of learnable parameters in a model. 

At the compilation step:

- **loss function**: (or objective function) measures the performance of the model on the training data by comparing the observed and the expected output. The distance score will be used as a feedback in order to adjust and improve the model (= learning). 
- **optimizer**: mechanism through which the network updates itself based on the data and the loss function. It implements backpropagation in order to find which are the weights that minimize the loss function (distance score).
- **metrics**: e.g. **accuracy** which is the fraction of correct predictions.


During the training:

- **fitting**: fit of the model to the training data.
- **overfitting**: when there a gap between the train and the test accuracy. The model performs worse on new data. 
- **information leaks**: when everytime a hyperparameter is tuned based on the model's performance on the validation set, some information about the validation data leaks into the model.

## Data representations

- **tensor**: a multidimentional Numpy array (a container of numerical data). It is the basic data structure in machine learning.
- **rank**: the number of axes (== dimensions) in a tensor
- **scalar**: a zero-dimensional tensor. It contains only one value.
- **vector**: a one-dimensional tensor or array of numbers. It has one axis. A vector with n entries is a n-dimensional vector (NOTE: a nD tensor is not the same as a nD vector!).
- **matrix**: an array of vectors or 2D tensor.
- **shape**: a tuple that contains the number of dimensions along each axis. For a vector: `(n,)` and for a scalar: `()`.
- **samples axis**: the first axis (axis=0) of the data tensor.
- **batch**: a sub-group of the dataset that is processed by the model. It is usually a power of 2, to facilitate memory allocation on GPU. 

## Operations

Neural networks consist of chains of tensor operations that are geometric transformations of the input data.

- **broadcasting**: when manipulating tensors with different shapes, first axes (broadcast axes) are added to the smaller tensor in order to match the ndim of the larger one and second, the smaller tensor is repeated alongside these new axes.
- **gradient**: is the derivative of a tensor operation. It is the generalization of a derivative to functions of multidimentional inputs (tensors).

Updating the weights: 

All operations in a network are differentiable and compute the gradient of the loss with respect to the weight coefficients. By moving the coefficients in the opposite direction of the gradient, the loss is decreased. For example: `W1 = W0 - step * gradient(f)(W0)`. That means going against the curvature, which intuitively should put you lower on the curve.

- **stochastic gradient descent**: the procedure of finding the all the points where the derivative goes to zero and then finding for which the function has the lowest value. It the case of a neural network, we find analytically the combination of weight values that give the smallest loss function, by `gradient(f)(W) = 0`. Because this is impossible to solve analytically for a large number of coefficients, we compute the loss from a batch and the gradient of the loss with respect to the parameters = **backward pass**. Then we move the parameters in the opposite direction from the gradient `W -= step * gradient` and thus lower the loss. It is called stochastic because each bach is drawn randomly. The step is called **learning rate**.
- **learning rate**: It is the factor by which the parameters are moved on the opposite side of the gradient. If it is too small the descent will take too long and could be stuck at a local minimum. If it is too large, we could end up at random points on the curve.

There are many variations of SGD that differ by taking into account previous weight updates (e.g. Adagrad, RMSProp). They are known as **optimizers**. The SGD with momentum address the convergence speed and local minima. The momentum helps to avoid being stuck at local minima. 

- **activation functions**: functions that introduce non-linearity.
- **relu**: (rectified linear unit) is a function that can zero out negative values.
- **sigmoid**: it translates arbitari unints into the [0,1] range, which is useful for probabilistic interpretations.
- **softmax**: commonly used in multiclass classification problems since it outputs a probability distribution over the N output classes.
- **cross entropy**: is a quantity that measures the distance between probability distributions or between the ground truth and the predictions.
- **sparse categorical crossentropy**: when encoding the labels as integers.
- **one-hot-encoding**: encoding the labels via categorical encoding. In this case the MSE (mean squared error) loss function and the MAE (mean absolute metric) metric are applied. When features in the input data have values in different ranges, each feature should be scaled independently as a preprocessing step.
- **regression**: when the model predicts a continuous value instead of a discrete one.
- **K-fold cross validation**: it is used when having too few data points. It consists of splitting the available data into K partitions (typically K = 4 or 5), instanti- ating K identical models, and training each one on K – 1 partitions while evaluating on the remaining partition. The validation score for the model used is then the average of the K validation scores obtained.

## Branches of machine learning

- **supervised learning**: the goal is to learn the relationship between training inputs and training targets (annotations). Example categories: sequence generation, syntax tree prediction, object detection and image segmentation.
- **unsupervised learning**: it finds interesting transformations of the input data without the help of any targets. Example categories: dimensionality reduction and clustering.
- **self-supervised learning**: it is supervised learning without human-annotated labels. The labels are generated from the input data using a heuristic algorithm. Example: autoencoders. 
- **reinforcement learning**: an agent receives information about its environment and learns to choose actions that will maximize some reward

NOTES:

The goal is to achieve models that generalize, which means that they perform well on never been seen before data (the test data) while minimizing the overfitting. The model must be trained and validated and once it performs well  it can be tested. You usually should randomly shuffle your data before splitting it into training and test sets (unless the data follow the time/future predictions). Make sure your training set and validation set are disjoint.

- **optimization**: the process of adjusting a model to get the best performance possible on the training data.
- **generalization**: refers to how well the trained model performs on data it has never seen before.

At the beginning of training, optimization and generalization are correlated: the lower the loss on training data, the lower the loss on test data. While this is happening, your model is said to be **underfit**: there is still progress to be made; the network hasn’t yet modeled all relevant patterns in the training data. To prevent a model from learning misleading or irrelevant patterns found in the training data (**overfit**), the best solution is to get more training data. A model trained on more data will naturally generalize better. When that isn’t possible, the next-best solution is to modulate the quantity of information that your model is allowed to store or to add constraints on what information it’s allowed to store (**regularization**). 

## Regularization techniques

- **reducing the network's size**: reduce the number of learnable parameters in the model (determined by the number of layers and the number of units per layer). The smaller network (smaller capacity) starts overfitting later than the reference network  and its performance degrades more slowly once it starts overfitting. The bigger network starts overfitting almost immediately, after just one epoch, and it overfits much more severely. Its validation loss is also noisier.
- **adding weight regularization**: a common way to mitigate overfitting is to put constraints on the complexity of a network by forcing its weights to take only small values, which makes the distribution of weight values more regular. It’s done by adding to the loss function of the network a cost associated with having large weights. There are two options: L1 regularization—the cost added is proportional to the absolute value of the weight coefficients and L2 regularization (or weight decay)—The cost added is proportional to the square of the value of the weight coefficients. In Keras, weight regularization is added by passing weight regularizer instances to layers as keyword arguments:
```python
from keras import regularizers

model = models.Sequential()
model.add(layers.Dense(16, kernel_regularizer=regularizers.l2(0.001),
activation='relu', input_shape=(10000,))) model.add(layers.Dense(16, kernel_regularizer=regularizers.l2(0.001),activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
```
In this example every coefficient in the weight matrix of the layer will add `0.001 * weight_coefficient_value` to the total loss of the network. Note that because this penalty is only added at training time, the loss for this network will be much higher at training than at test time.

Other option:
```python
from keras import regularizers

regularizers.l1(0.001)
regularizers.l1_l2(l1=0.001, l2=0.001)
```
- **adding dropout**: applied to a layer, consists of randomly dropping out (setting to zero) a number of output features of the layer during training. The dropout rate is the fraction of the features that are zeroed out; it’s usually set between 0.2 and 0.5.  At test time, no units are dropped out; instead, the layer’s output values are scaled down by a factor equal to the dropout rate, to balance for the fact that more units are active than at training time.
At training time:
```python3
layer_output *= np.random.randint(0, high=2, size=layer_output.shape)
```
The above will drop 50% of the units in the output.

At test time:
```python
layer_output *= 0.5
```
The core idea is that introducing noise in the output values of a layer can break up happenstance patterns that aren’t significant, which the network will start memorizing if no noise is present. In Keras:
```python3
model.add(layers.Dropout(0.5))
``` 


## Evaluations

### Simple hold-out validation

Set apart some fraction of your data as your test set. Train on the remaining data, and evaluate on the test set. You shouldn’t tune your model based on the test set, and therefore you should also reserve a validation set.
```python
num_validation_samples = 10000
np.random.shuffle(data)

validation_data = data[:num_validation_samples]
data = data[num_validation_samples:]
training_data = data[:]

model = get_model()
model.train(training_data)
validation_score = model.evaluate(validation_data)

model = get_model()
model.train(np.concatenate([training_data,validation_data]))
test_score = model.evaluate(test_data)
```

### K-fold validation

With this approach, you split your data into K partitions of equal size. For each partition i, train a model on the remaining K – 1 partitions, and evaluate it on partition i. Your final score is then the averages of the K scores obtained.

```python
k=4
num_validation_samples = len(data) // k
np.random.shuffle(data)
validation_scores = [] 

for fold in range(k):
    validation_data = data[num_validation_samples * fold: num_validation_samples * (fold + 1)]
    training_data = data[:num_validation_samples * fold] + data[num_validation_samples * (fold + 1):]
    model = get_model()
    model.train(training_data)
    validation_score = model.evaluate(validation_data) 
    validation_scores.append(validation_score)       

validation_score = np.average(validation_scores)
model = get_model()
model.train(data)
test_score = model.evaluate(test_data)
```

### Iterated K-fold validation with shuffling

When the data are too few and you need to evaluate your model as precisely as possible. It consists of applying K-fold validation multiple times, shuffling the data every time before splitting it K ways. The final score is the average of the scores obtained at each run of K-fold validation.

## Data preprocessing

- **vectorization**: all inputs and targets in a neural network must be tensors of floating-point data (or integers).
- **value normalization**: it isn’t safe to feed into a neural network data that takes relatively large values or data that is heterogeneous since it can trigger large gradient updates that will prevent the network from converging. In the most strict case, the data should have a mean of and a standard deviation of 1.
- **handling missing values**: it’s safe to input missing values as 0, with the condition that 0 isn’t already a meaningful value. The network will learn from exposure to the data that the value 0 means missing data and will start ignoring the value. You should artificially generate training samples with missing entries: copy some training samples several times, and drop some of the features that you expect are likely to be missing in the test data.
- **feature engineering**: is the process of using your own knowledge about the data and about the machine learning algorithm at hand to make the algorithm work better by applying hardcoded (nonlearned) transformations to the data before it goes into the model.

Summary of choices:

| Problem type | Last-layer activation  |  Loss function |   
|---|---|---|
| Binary classification  | sigmoid  | binary_crossentropy |     
| Multiclass, single-label classification  |  softmax |  categorical_crossentropy |      
| Multiclass, multilabel classification | sigmoid  | binary_crossentropy |      
| Regression to arbitrary values | None  | mse |
| Regression to values between 0 and 1 | sigmoid | mse or binary_crossentropy  |

# Applications

## Computer vision

### convolutional neural networks

It is a type of deep learning model used in computer vision applications, such as image classification problems. They take as inputs tensors of shape `image_height, image_width, image_channels)`. The width and height dimensions tend to shrink going deeper in the network.
Example of a small convnet:
```python
model = models.Sequential()
model.add(layers.Conv2D(32,(3,3), activation='relu', input_shape=(28,28,1))) 
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64,(3,3), activation='relu')) 
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64,(3,3), activation='relu'))
```
NOTES:
- `Dense` layers learn global patterns in their input feature space, while convolution layers learn local patterns (patterns found in small 2D win- dows of the inputs) such as edges and textures.
- The patterns they learn are translation invariant.
- They can learn spatial hierarchies of patterns.
- Convolutions operate over 3D tensors, called **feature maps**, with two spatial axes (height and width) as well as a **depth axis** (also called the channels axis). For an RGB image, the dimension of the depth axis is 3, because the image has three color channels: red, green, and blue. For a black-and-white picture, like the MNIST digits, the depth is 1 (levels of gray).
- The convolution operation extracts patches from its input feature map and applies the same transformation to all of these patches, producing an **output feature map**. This output feature map is still a 3D tensor: it has a width and a height. Its depth can be arbitrary, since it stands for filters.
- **Filters** encode specific aspects of the input data. For example the output feature map is (26,26,32) where there are 32 filters (32 output channels with 26x26 grid values), which is a **response map** of the filter over the input, indicating the response of that filter pattern at different locations in the input. That is what the term feature map means: every dimension in the depth axis is a feature (or filter), and the 2D tensor `output[:, :, n]` is the 2D spatial map of the response of this filter over the input.

The key parameters that define convolution are:
- Size of the patches extracted from the inputs
- Depth of the output feature map

Syntax:
```python
Conv2D(output_depth, (window_height, window_width))
```
A convolution works by sliding these windows of size 3 × 3 or 5 × 5 over the 3D input feature map, stopping at every possible location, and extracting the 3D patch of surounding features (shape `(window_height, window_width, input_depth)`). Each such 3D patch is then transformed (via a tensor product with the same learned weight matrix, called the **convolution kernel**) into a 1D vector of shape `(output_depth,)`. All of these vectors are then spatially reassembled into a 3D output map of shape `(height, width, output_depth)`.

