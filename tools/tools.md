# Tools

## Progress bars
Imports:
```python
from tqdm import tqdm
```
To print a progress bar at a for loop:
```python
for i in enumerate(tqdm(list)):
    ...
```

## Timing functions

Imports:
```python
import time
```
Include the following function that will print the time:
```python
def timeit(func):
    def timed(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print('Function', func.__name__, 'time:', round((end - start) * 1000, 1), 'ms')
        return result
    return timed
```
and add it as a decorator to the function you want to time. For example:
```python
@timeit
def function():
    ...
```
