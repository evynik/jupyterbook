# Slurm

Slurm is an open source, fault-tolerant, and highly scalable cluster management and job scheduling system for large and small Linux clusters.

## Job monitoring

To display the occupancy of the computing platform:
```
sjstat
```

For the job status (with less or more details):
```
squeue
sacct
```
To delete jobs:
```
scancel <jobid>
scancel -u <username>
```

## Useful commands

To show more characters of the job name:
```
squeue --format="%.18i %.9P %.30j %.8u %.8T %.10M %.9l %.6D %R" --me
``` 
Here it will print the first 30 characters with `%.30j`

To count the number of jobs that are running:
```
squeue -h -t running -r -O "state" | uniq -c
```

## Job template

General job submission script.

```
sbatch_memory        = 4000 # Memory in MB
sbatch_time          = '0-20:00:00'


def get_job_file(job_name, log_name, in_file, out_file, pwd, memory=sbatch_memory, time=sbatch_time):
    context = {
           'job_name':    job_name,
           'log_name':    log_name,
           'in_file':     in_file,
           'out_file':    out_file,
           'pwd':         pwd,
           'memory':      memory,
           'time':        time,
          
    }
    template = """#!/bin/sh
# SLURM options:
#SBATCH --job-name={job_name}     # Job name
#SBATCH --output={log_name}       # Standard output and error log
#SBATCH --partition htc           # Partition choice
#SBATCH --ntasks 20               # Run a single task (by default tasks == CPU)
#SBATCH --mem {memory}            # Memory in MB per default
#SBATCH --time {time}             # 7 days by default on htc partition

#module load /sps/nusol/software/miniconda3/bin/conda
source activate
#conda activate /sps/nusol/software/miniconda3/envs/ds
#conda env list
source /sps/nusol/software/set_environment.sh

cd {pwd}

python3 file.py -i {in_file} -o {out_file}

"""
    return template.format(**context)

pwd = os.getenv('PWD')
if not os.path.exists('outfolder'): os.mkdir('outfolder')
job_dir = f'{pwd}/outfolder'

path = Path('folder').absolute()
in_files = np.sort(glob.glob(f'{path}/*'))

jobs = len(in_files)

for j in np.arange(jobs):

    file_name = os.path.split(in_files[j])[-1]
    file_name = ''.join(file_name.split("_")[0:2])

    job_tag_name = f'{job_dir}/job_{file_name}.sh'
    log_tag_name = f'{job_dir}/log_{file_name}.log'
    
    job = get_job_file(job_name=job_tag_name, log_name=log_tag_name, in_file=in_files[j], out_file=file_name+'.root', pwd=pwd)
    with open(job_tag_name, 'w') as f:
        for l in job: f.write(l)

    print(job_tag_name)

    os.system(f'/bin/chmod +x {job_tag_name}')
    os.system(f'sbatch -L sps {job_tag_name}')

```
