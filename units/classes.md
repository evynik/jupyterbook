# Python classes

A class is a form of data type. It contains data = **attributes** (names following a dot) and functions = **methods**.

An *instance* of a class is called an **object**:
```
i = class()
```
i is the object of the type class.

The basic syntax of a class is:
```
class example:

    def __init__(self, a, b):
        self.a = a
        self.b = b
        self.c = ...

    def function(self):
        return self.a + self.b

x = example(1,2)

print(x.a)
print(x.function())
```

The ``` __init__ ``` method is called **constructor** and it initialises the class attributes when an instance of the class is created. The ```self``` argument must be passed first in every method as it represents the instance of the class. With it we can access the classe's attributes. 

## Namespaces and scopes

Some definitions:

**namespace** = a mapping from names to objects. NOTE: There is no relation between names in different namespaces.

Namespaces are created at different moments and have different lifetimes. The namespace containing the built-in names is created when the Python interpreter starts up, and is never deleted. The global namespace for a module is created when the module definition is read in; normally, module namespaces also last until the interpreter quits. The statements executed by the top-level invocation of the interpreter, either read from a script file or interactively, are considered part of a module called __main__, so they have their own global namespace. (The built-in names actually also live in a module; this is called builtins.) The local namespace for a function is created when the function is called, and deleted when the function returns or raises an exception that is not handled within the function. 

**scope** = a textual region of a Python program where a namespace is directly accessible. An unqualified reference to a name attempts to find the name in the namespace.

Although scopes are determined statically, they are used dynamically. At any time during execution, there are 3 or 4 nested scopes whose namespaces are directly accessible:

- the innermost scope, which is searched first, contains the local names
- the scopes of any enclosing functions, which are searched starting with the nearest enclosing scope, contain non-local, but also non-global names
- the next-to-last scope contains the current module’s global names
- the outermost scope (searched last) is the namespace containing built-in names

If a name is declared global, then all references and assignments go directly to the next-to-last scope containing the module’s global names. To rebind variables found outside of the innermost scope, the nonlocal statement can be used; if not declared nonlocal, those variables are read-only (an attempt to write to such a variable will simply create a new local variable in the innermost scope, leaving the identically named outer variable unchanged). Usually, the local scope references the local names of the (textually) current function. Outside functions, the local scope references the same namespace as the global scope: the module’s namespace. Class definitions place yet another namespace in the local scope.

f no global or nonlocal statement is in effect, assignments to names always go into the innermost scope. Assignments do not copy data — they just bind names to objects. The same is true for deletions. All operations that introduce new names use the local scope: in particular, import statements and function definitions bind the module or function name in the local scope.