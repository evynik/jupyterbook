# Logging

The logging module in python is useful for printing information during the execution of a script or writting log files to keep track of the conditions of the script. It can include both user and third-party module messages.

Basic classes defined by the module:

- loggers

Expose the interface that application code directly uses. 
- handlers

Send the log records (created by loggers) to the appropriate destination.
- filters

Provide a finer grained facility for determining which log records to output.
- formatters

Specify the layout of log records in the final output.

Create a module-level logger with:
```python
import logging

logger = logging.getLogger(__name__)
```
Logged messages to the module-level logger get forwarded to handlers of loggers in higher-level modules, all the way up to the highest level logger, the root logger. This is hierarchical logging.

Multiple calls to getLogger() with the same name will always return a reference to the same Logger object.  `__name__` is the module’s name in the Python package namespace.


Setup root-logger configuration:
```python
logging.basicConfig(filename='output.log', level=logging.INFO)

```
or you can use commands separately:
```python
logger.setLevel(logging.DEBUG)
```

## Levels

- NOTSET
- DEBUG
- INFO
- WARNING 
- ERROR 
- CRITICAL 

Add something to the logger:
```python
logger.info('Started')
```
This will print:
```
INFO:__main__:Started
```
Other message options are:
```python
logger.debug()
logger.warning()
logger.error()
logger.critical()
logger.log()
logger.exception()
```

## Handlers

Creating and adding handlers to the logger:
```python
file_handler = logging.FileHandler(log_path, 'w+')
logger.addHandler(file_handler)
```
to remove:
```python
logger.removeHandler()
```

## Filters

Can be used by Handlers and Loggers for more sophisticated filtering than is provided by levels. The base filter class only allows events which are below a certain point in the logger hierarchy.

Adding/removing filters:
```python
logger.addFilter()
logger.removeFilter()
```

## Formatters 

Formatters convert a LogRecord to an output string that can be interpreted by a human or an external system.