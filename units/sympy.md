# SymPy

SymPy is a Python library for symbolic mathematics (the mathematical objects are represented exactly, not approximately, and mathematical expressions with unevaluated variables are left in symbolic form).

```python
import sympy 
from sympy import *
init_printing(use_unicode=True)
```

-  in SymPy, variables are defined using symbols and they must be declared before they are used:

```python
x, y = symbols('x y')
t = Symbol('t', positive=True, integer=True)
```

Symbols are NOT Python variables. To pass a value to a symbol we do the following:

```
x = symbols('x')
expr = x + 1
expr.subs(x, 2)
```
This will substitude the symbol `x` with 2, so that `expr = 3`.

NOTE: SymPy objects are immutable. This means that expr will always be `x + 1` even after the `subs` operation.

To change multiple symbols:

```python
expr = x**3 + 4*x*y - z
expr.subs([(x, 2), (y, 4), (z, 0)])
```

To convert a string to a SymPy object use `sympify`
```python
str_expr = "x**2 + 3*x - 1/2"
expr = sympify(str_expr)
expr.subs(x, 2)
```
The best practice is to use evalf() to substitute numerical values into expressions.
```python
x = Symbol('x')
f = cos(x) - x
x_value = 0.7
f.evalf(subs={x: x_value})
```

### Evaluating

To evaluate an expression at many points with SymPy would be very slow. Instead, you should use libraries like NumPy and SciPy. The easiest way to convert a SymPy expression to an expression that can be numerically evaluated is to use the `lambdify` function. `lambdify` acts like a lambda function, except it converts the SymPy names to the names of the given numerical library, usually NumPy. 

For example:
```python
a = numpy.arange(10) 
expr = sin(x)

f = lambdify(x, expr, "numpy") 
print(f(a)) 
```

## Math

Various operations are:
```python
diff(sin(x)*exp(x), x)

integrate(exp(x)*sin(x) + exp(x)*cos(x), x)
integrate(sin(x**2), (x, -oo, oo))

limit(sin(x)/x, x, 0)

solve(x**2 - 2, x)

Matrix([[1, 2], [2, 2]]).eigenvals()

expr.series(x, 0, 4)
```

Special functions:
```python
factorial(n)
binomial(n, k)
gamma(z)
```

## ODEs

SymPy can solve in a very simple way ordinary differential equations.

First we define the function and the equation we need to solve:
```python
t, k, N0 = symbols('t k N0')
f = symbols('f', cls=Function)

eq = Eq(f(t).diff(t), -k*f(t))
sol = dsolve(eq, f(t), ics={f(0):N0})
pprint(sol.rhs)
```
`Eq` is a symbolic equality. A second derivative would be: `f(t).diff(t, t)`

To check that a solution is correct:
```python
checkodesol(eq, sol)
```