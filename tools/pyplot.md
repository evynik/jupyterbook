# Matplotlib

Set back to default matplotlib parameters:
```python
import matplotlib

matplotlib.rcParams.update(matplotlib.rcParamsDefault)
```

Set fontsize:
```python
matplotlib.rcParams.update({'font.size': 12})
```

Set figure size:
```python
plt.rcParams['figure.figsize'] = (9,8)
```
