# uproot
```
import uproot
```
To simply open a .root file with uproot:
```
f = uproot.open("file.root")
```
To access a tree and its leafs:
```
tree = f['tree'].arrays(['leaf1', 'leaf2', ...])

a = tree['leaf1']
```
