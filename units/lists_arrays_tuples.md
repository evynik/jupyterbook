# Objects and control flows

# Functions

Remarks:

- The first statement of the function body can optionally be a string literal; this string literal is the function’s documentation string, or docstring. Print it with:
```python
print(my_function.__doc__)
```
- We can specify default values for some of the arguments, so that the function can be called with less arguments. The default values are evaluated only once.
- Functions can be called using keyword arguments (kwarg=value). Keyword arguments must always follow the positional arguments. Example:
```python 
def func(x, *arguments, **keywords):
    for arg in arguments:
        print(arg)
    for kw in keywords:
        print(kw, ":", keywords[kw])
```
- If / and * are not present in the function definition, arguments may be passed to a function by position or by keyword. Example:
```python
def f(pos1, pos2, /, pos_or_kwd, *, kwd1, kwd2):
    pass

def pos_only_arg(arg, /):
    print(arg)

def kwd_only_arg(*, arg):
    print(arg)

def combined_example(pos_only, /, standard, *, kwd_only):
    print(pos_only, standard, kwd_only)
```
- A function can be called with an arbitrary number of arguments. These arguments will be wrapped up in a tuple.
```python
def concat(*args, sep="/"):
    return sep.join(args)

concat("earth", "mars", "venus")
```
- The reverse situation occurs when the arguments are already in a list or tuple but need to be unpacked for a function call requiring separate positional arguments.  If they are not available separately, write the function call with the *-operator to unpack the arguments out of a list or tuple. dictionaries can deliver keyword arguments with the **-operator.
- Small anonymous functions can be created with the **lambda** keyword. For example, this function returns the sum of its two arguments: `lambda a, b: a+b`. Lambda functions can be used wherever function objects are required. They are syntactically restricted to a single expression.
```python
def make_incrementor(n):
    return lambda x: x + n

f = make_incrementor(42)
f(0) # 42
f(1) # 43
```
- Function annotations are completely optional metadata information about the types used by user-defined functions. They are stored in the __annotations__ attribute of the function as a dictionary and have no effect on any other part of the function.
```python
def f(x: int, s: str)->str:
    print(f.__annotations__)
    a = x + 2
    return s
```
Coding style manual: https://peps.python.org/pep-0008/

# Lists

A list can hold different types of elements. Some useful commands are:

- Last element of the list:
```python
list[-1]
```
- To slice a list:
```python
list[1:3]
```
this will return the second to fourth element.

You can clear a list by replacing all the elements with an empty list:
```python
list[:] = []
```

- To get the first or last n elements:
```python
list[:n]
list[-n:]
```
If we need to create a new list that we populate inside a for loop, instead of creating an empty list and then using the ```.append()``` method, it is better to do for example the following:

```python
list = [None]*len(a)

for i in np.arange(a):
    list[i] = 1
```
This is faster and better for memory management.

To convert an array of strings into a single string:
```python
' '.join(list)
```
and for a list of numbers:
```python
' '.join(str(n) for n in list)
```

To remove a given index (or slice) of a list:
```python
del a[0]
```

## List methods

Some useful list methods are:
```python
l.append(x)
l.extend(iterable)
l.insert(i,x)
l.remove(x)
l.pop([i])
l.clear()
l.index(x[,start[,end]])
l.count(x)
l.sort(*, key=None, reverse=False)
l.reverse()
l.copy()
```
extend: extends the list by appending all the items from the iterable

pop: removes the item at the given position in the list, and returns it. If no index is specified, it removes and returns the last item in the list

index: returns zero-based index in the list of the first item whose value is equal to x

## List comprehension

Some examples of making more consise code and avoiding for loops:

```python
squares = [x**2 for x in range(10)]
squares = list(map(lambda x: x**2, range(10)))

[(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]

[[row[i] for row in matrix] for i in range(4)]
```

# Tuples

Tuples hold elements inside () and they are unmutable, which means that they can not be modified in any way. It also does not support indexing.

A tuple consists of a number of values separated by commas:
```python
t = 12345, 54321, 'hello!'
a = t[0]

u = t, (1, 2, 3, 4, 5)

v = ([1, 2, 3], [3, 2, 1])
```
 It is not possible to assign to the individual items of a tuple, however it is possible to create tuples which contain mutable objects, such as lists.

NOTE: Empty tuples are constructed by an empty pair of parentheses; a tuple with one item is constructed by following a value with a comma (it is not sufficient to enclose a single value in parentheses).

# Sets

A set is an unordered collection with no duplicate elements.  Basic uses include membership testing and eliminating duplicate entries.

Set elements are inside {}. Sets store unordered and unique items. So it is True that:
```python
{1,2,3} == {3,2,1}
```
For this reason we cannot access its elements with an index.

NOTE: to create an empty set you have to use `set(...)`

Example:
```python
s = {'a','b','c'}
'c' in s # True

s = set('apple')
>>> {'a','p','l','e'} # only unique elements
```

# Strings

Strings are immutable objects, which means that they cannot be changed (eg. changing an index). 

- To convert all to upper/lowercase:
```python
s = s.upper()
s = s.lower()
```
- To seperate the words of a sentence
```python
s.split()
```
The empty parenthesis means that it is splitted by the gaps.

- To count the occurances of an element in a string
```python
s.count("a")
```
### String formatting

Sometimes we want to modify strings or add variables to them. This is done with the ``` f'...{}...' ``` or the ```.format()``` method. For example:
```python
s = f'This is {var1} and {var2}.'

print("Hello {}.".format("you"))
```
Options for printing:
```python
{n:.3f} # 3 decimals, float
```
Passing an integer after the ':' will cause that field to be a minimum number of characters wide. This is useful for making columns line up.

Other modifiers can be used to convert the value before it is formatted. `!a` applies ascii(), `!s` applies str(), and `!r` applies repr(). The = specifier can be used to expand an expression to the text of the expression, an equal sign, then the representation of the evaluated expression
```python

```

# Dictionaries

A dictionary is contained between {} and holds key-value pairs.

The general format is:

```python
dict = {
    "key1": value1,
    "key2": value2,
    "key3": value3
}
```
To see just the keys:
```python
list(dict)
```
To check if the key is in the dictionary:
```python
'key' in dict
```
To access a value
```python
dict["key1"]
```
To delete a key-value pair
```python
del dict["key1"]
```
To loop over the elements of a dictionary
```python
for key, value in dict.items():
    dict['key'] = a
```

To combine dictionaries use
```python
new_dict = {**dict1, **dict2, ...}
```


# Arrays

Numpy arrays hold numerical values of the ***same type***. In general they are n-dimensional.

To convert a list into an array:
```python
a = np.array(list)
```

## Creating arrays

### np.arange() and np.linspace()

This functions create a one-dimentional array given a range and an optional step. 
```python
np.arange(0, 11, 2)
np.lispace(0, 10, 5)
```
np.arange() returns an array from 0 to 10 with step 2 and np.linspace() returns 5 equally spaced points between 0 and 10.

# Awkward arrays

Documentation:

https://awkward-array.org/doc/main/user-guide/index.html

```python
import awkward as ak
```

To get the mean of each subarray in an awkward array:
```python
mean_z = ak.mean(zs, axis=-1)
```

# Control flows

## Match

A match statement takes an expression and compares its value to successive patterns given as one or more case blocks.  If no case matches, none of the branches is executed.

```python
def func(number):
    match status:
        case 400:
            return 0
        case 404:
            return "Not found"
        case 418:
            return 1
        case _:
            return "Something's wrong"
```
It can be more advanced:
```python
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def where_is(point):
    match point:
        case Point(x=0, y=0):
            print("Origin")
        case Point(x=0, y=y):
            print(f"Y={y}")
        case Point(x=x, y=0):
            print(f"X={x}")
        case Point():
            print("Somewhere else")
        case _:
            print("Not a point")
```
A recommended way to read patterns is to look at them as an extended form of what you would put on the left of an assignment, to understand which variables would be set to what. Subpatterns may be captured using the as keyword:
```python
case (Point(x1, y1), Point(x2, y2) as p2): ...
```
