# Modules

Python has a way to put definitions in a file and use them in a script or in an interactive instance of the interpreter. Such a file is called a module; definitions from a module can be imported into other modules or into the main module. A module is a file containing Python definitions and statements. Within a module, the module’s name (as a string) is available as the value of the global variable `__name__`.

A module can contain executable statements as well as function definitions. These statements are intended to initialize the module. They are executed only the first time the module name is encountered in an import statement. The imported module names, if placed at the top level of a module (outside any functions or classes), are added to the module’s global namespace.

`import module'` does not add the names of the functions defined in module, so refer to them as: `module.name()` or use `from module import *`. This imports all names except those beginning with an underscore (_).

To find out the names that a module includes do:
```python
dir(module)
```
Without arguments, dir() lists the names you have defined currently (variables, modules, functions, etc but not the build-ins).

## Usable modules

To make a module usable as a script (the `__name__` is set to `__main__`):
```python
python module.py <arguments>
```
add the following at the end of the module:
```python
if __name__ == "__main__":
    import sys
    function(int(sys.argv[1]))
```
This code that parses the command line only runs if the module is executed as the “main” file.

## Search paths

When a module is imported, the interpreter first searches for a built-in module with that name at `sys.builtin_module_names`. Then, it searches in a list of directories given by the variable `sys.path` which is initialized from these locations:
- directory of the input script
- PYTHONPATH
- the installation-dependent default

NOTE: To speed up loading modules, Python caches the compiled version of each module in the `__pycache__` directory under the name `module.version.pyc`, where the version encodes the format of the compiled file; it generally contains the Python version number.

https://peps.python.org/pep-3147/

The variable `sys.path` is a list of strings that determines the interpreter’s search path for modules. It is initialized to a default path taken from the environment variable PYTHONPATH, or from a built-in default if PYTHONPATH is not set. You can modify it using standard list operations:
```python
import sys
sys.path.append('...')
```

## Packages

Packages (collection of modules) are a way of structuring Python’s module namespace by using “dotted module names”. For example, the module name A.B designates a submodule named B in a package named A. 

When importing the package, Python searches through the directories on `sys.path` looking for the package subdirectory. The `__init__.py` files are required to make Python treat directories containing the file as packages. `__init__.py` can just be an empty file, but it can also execute initialization code for the package or set the `__all__` variable.  

If a package’s `__init__.py` code defines a list named `__all__`, it is taken to be the list of module names that should be imported when from package `import *` is encountered.

You can use relative imports:
```pyhton
from . import module1
from .. import module2
from ..module import module3
```


